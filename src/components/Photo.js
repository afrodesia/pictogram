import React, { Component } from 'react'

export default class Photo extends Component {
  render() {
    const post = this.props.post
    return (
      <figure className="photowall">
        <div className="image-const">
          <img src={post.imageLink} alt={post.description}/>
        </div>
         <h2>{post.description}</h2>
      </figure>
    )
  }
}
