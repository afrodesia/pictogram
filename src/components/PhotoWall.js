import React, { Component } from 'react'
import Photo from './Photo'


export default class PhotoWall extends Component {


  render() {
    return (
      <div className="photo-wrap">
        {this.props.posts.map((post, index) => <Photo key={index} post={post}/>)}
      </div>
    )
  }
}
