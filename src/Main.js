import React, { Component } from 'react'
import Title from './components/Title'
import PhotoWall from './components/PhotoWall'

const posts = [
  {
    id:0,
    description: "Chea me out",
    imageLink: 'https://img.discogs.com/6vXRbLu0O3Lfyie3-OIep7y2_BQ=/fit-in/300x300/filters:strip_icc():format(jpeg):mode_rgb():quality(40)/discogs-images/A-5078245-1464661525-4025.jpeg.jpg'
  },
  {
    id:1,
    description: "The Heari",
    imageLink: 'http://i1.jpopasia.com/assets/1/109-leehaeri-cli8.jpg'
  },
  {
    id:2,
    description: "Lee H",
    imageLink: 'https://www.allkpop.com/upload/2017/10/af_org/04210728/Davichi-lee-hae-ri.jpg'
  },

]

export default class Main extends Component {
  render() {
    return (
      <div className="App">
        <Title title={'Photowall'}/>
          <PhotoWall posts={posts}/>
        
      </div>
    )
  }
}
